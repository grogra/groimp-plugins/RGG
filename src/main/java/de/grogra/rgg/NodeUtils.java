package de.grogra.rgg;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.State;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.persistence.Transaction;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.TypeItem;
import de.grogra.reflect.Type;

public class NodeUtils {
	
	/**
	 * This method replace the nodes that are kept during project reset (i.e. the nodes under the root, but
	 * not the RGGRoot) with newer version of their compiled types. It should only affect custom types (e.g. 
	 * user defined modules from within the project). By default, the old nodes use their full type name 
	 * (e.g. Model.A for module A in file Model). The parameter unstableNaming however forces the use of only
	 * the module name (e.g. A). It can lead to nodes being mapped to the wrong new type (e.g. if the project 
	 * includes Model.A and Otherfile.A, the new node of type A will one "randomly"). 
	 * 
	 * The parameter unstableNaming is useful in the cases where: - the module definition changed file.
	 * - Each module name is unique.
	 * 
	 * @param root the node where the replacement starts
	 * @param r the current registry to use (where the compiled classes are stored)
	 * @param xa the current active transaction. 
	 * @param unstableNaming if true the new types only check the module name, not its complete name.
	 */
	public static void replaceOldExtent(Node root, Registry r, Transaction xa, boolean unstableNaming) {
		// optional - replace all existing nodes (under RootNode - above rggroot)
		// to their newly compiled classes - should be done before init
	    Set<Node> newNode = new HashSet<Node>();
		Item classes_directory = r.getItem("/classes");
	    Item[] classes = Item.findAll(classes_directory, ItemCriterion.INSTANCE_OF, TypeItem.class, true);
	    HashMap<String, Type> modelTypes = new HashMap<String, Type>();
	    for (Item type: classes) {
	    	modelTypes.put(type.getName(), (Type) ((TypeItem)type).getObject());  
	    }
	    // replace old model types visitor
	    VisitorImpl v = new VisitorImpl ()
		{
			@Override
			public Object visitEnter (Path path, boolean node)
			{
				if (node)
				{
					Node n = (Node) path.getObject (-1);
					if (newNode.contains(n)) {return null;}
					Type t = null;
					if (unstableNaming) {
						for (Entry<String, Type> e : modelTypes.entrySet()) {
						    if (e.getKey().endsWith(adaptMethodName(n.getNType().getName()))) {
						    	t=e.getValue();
						    	break;
						    }
						}
					}
					else {
						t = modelTypes. get(adaptMethodName(n.getNType().getName()));
					}
//					
					if (t != null ){
						try {
							Object o = t.newInstance();
							if (o instanceof Node) {
								try {
									((Node) o).dupFrom(n, true, xa);
									copyShaderAndTransform((Node)o, n);
									root.getGraph().replaceInGraph( (Node)o, n, xa);									
									newNode.add((Node)o);
								} catch (CloneNotSupportedException e) {
									e.printStackTrace();
								}
							}
						} catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
							e.printStackTrace();
						}
					}
				}
				return null;
			}
	
			@Override
			public Object visitInstanceEnter ()
			{
				return STOP;
			}
		};
		
		v.init (root.getCurrentGraphState(), EdgePatternImpl.TREE);
		root.getGraph().accept (root, v, null);
	}
	
	/**
	 * Change a method name from java name to binary name (e.g. Class.method to Class$method)
	 */
	private static String adaptMethodName(String name) {
		return name.replace('.', '$');
	}
	
	private static void copyShaderAndTransform(Node node, Node toCopy) {
		if ((toCopy instanceof ShadedNull) && (node instanceof ShadedNull) ) {
			((ShadedNull)node).setShader(((ShadedNull)toCopy).getShader());
		}
		
		if ((toCopy instanceof Null) && (node instanceof Null) ) {
			((Null)node).setTransform(((Null)toCopy).getLocalTransformation());
		}
	}

}
