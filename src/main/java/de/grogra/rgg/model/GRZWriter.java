package de.grogra.rgg.model;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.jar.JarOutputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.XMLGraphWriter;
import de.grogra.pf.io.DOMSource;
import de.grogra.pf.io.DOMSourceImpl;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.GraphXMLSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.OutputStreamSource;
import de.grogra.pf.io.PluginCollector;
import de.grogra.pf.io.StreamAdapter;
import de.grogra.pf.registry.Executable;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.XMLSerializer;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.rgg.RGG;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.vfs.FSFile;
import de.grogra.vfs.FileSystem;
import de.grogra.vfs.MemoryFileSystem;
import de.grogra.xl.lang.ObjectToBoolean;

/**
 * Grogra Resource zip. Contains the meta graph, a simplified version of the 
 * registry (only what is required to load the resources), and the explorers
 * resources. It is used by the Resource library to load resources from a project.
 */
public class GRZWriter extends FilterBase implements OutputStreamSource
{
	public static final IOFlavor FLAVOR
	= new IOFlavor (new MimeType ("application/x-grogra-resource+zip", null),
			IOFlavor.OUTPUT_STREAM, null);


	public GRZWriter (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (FLAVOR);
	}


	public void write (OutputStream out) throws IOException
	{
		Registry r = (Registry) ((ObjectSource) source).getObject ();		
		FileSystem fs = r.getFileSystem ();
		java.util.Collection files = r.getFiles ();
		fs.removeNonlistedAttributes (files);
		if (files.size()>0 && fs instanceof MemoryFileSystem)
		{
			((MemoryFileSystem) fs).removeNonlistedFiles (files);
		}
		else
		{
			r.substituteFileSystem (fs = new MemoryFileSystem (IO.PROJECT_FS));
		}
		//		

		final HashSet<FSFile> removedSourceFiles = new HashSet<FSFile>();
		write (r, fs, "project.gs", new XMLSerializer.Filter() {
			@Override
			public AttributesImpl filter(Item item, AttributesImpl attr)
			{
				if (item instanceof SourceFile)
				{
					SourceFile sf = (SourceFile) item;
					String mime = sf.getMimeType().getMediaType();
					boolean remove = false;
					if (mime.equals("text/x-java") || mime.equals("text/x-grogra-xl") || mime.equals("text/x-grogra-rgg"))
					{
						remove = true;
					}
					else if (mime.equals("application/x-grogra-rgg-compiled"))
					{
						remove = true;
					}
					if (remove)
					{
						removedSourceFiles.add(IO.toFile(item, sf.getSystemId()));
						return null;
					}
				}
				if (item.getName().contentEquals("workbench") || item.getName().contentEquals("meta") ) {
					return null;
				}
				return attr;
			}
		});
		final FileSystem fs2 = fs;
		JarOutputStream j = new JarOutputStream (out, fs.getManifest ());
		fs.writeJar (j, new byte[0x40000], fs.getRoot (), new ObjectToBoolean<Object>() {
			@Override
			public boolean evaluateBoolean(Object x)
			{
				return !removedSourceFiles.contains(new FSFile(fs2, x));
			}
		});
		j.flush ();
		j.close ();
	}

	public static void write (Registry reg, FileSystem fs, String projectFile, XMLSerializer.Filter filter)
			throws IOException
	{
		Collection plugins = new HashSet ();
		PluginCollector pc = new PluginCollector (plugins);

		//			IO.writeXML (new GraphXMLSource (reg.getProjectGraph (), reg, pc, 
		//					(de.grogra.graph.impl.Node) reg.getProjectGraph ().getRoot(GraphManager.META_GRAPH)),
		//						 fs, "graph.xml", GraphXMLSource.MIME_TYPE);



		IO.writeXML (new GraphXMLSource (reg.getProjectGraph (), reg, pc, 
				(de.grogra.graph.impl.Node) reg.getProjectGraph ().getRoot(GraphManager.META_GRAPH),
				new XMLGraphWriter.Filter() {
							@Override
							public AttributesImpl filter(de.grogra.graph.impl.Node node, Attributes attr) {
								if (node instanceof RGG) {
									return null;
								}
								return (AttributesImpl) attr;
							}
						}),
				fs, "graph.xml", GraphXMLSource.MIME_TYPE);


		// hook before writing fs to disk
		StringMap m = new StringMap ().putObject ("registry", reg);
		m.put("filesystem", fs);
		m.put("projectfile", projectFile);
		Executable.runExecutables (reg.getRootRegistry (), "/hooks/saving",
				reg, m);

		IOFlavor dom = new IOFlavor (Registry.MIME_TYPE, IOFlavor.DOM, null);
		DOMSource ds = new StreamAdapter (reg.createXMLSource (plugins, filter), dom);
		Document doc = ds.getDocument ();
		Element proj = doc.getDocumentElement ();
		proj.setAttribute ("graph", "graph.xml");
		Node r = proj.getFirstChild ();
		IO.writeXML (new DOMSourceImpl (doc, ds.getSystemId (), dom, reg, null),
				fs, projectFile, Registry.MIME_TYPE);
	}

}
