package de.grogra.rgg.model;

import java.util.Set;

import de.grogra.pf.registry.Registry;

class CompiledLib {

	final Registry registry;
	final String prefixes;
	final Set imports;
	CompiledLib(Registry r, String pref, Set i)
	{
		registry = r;
		prefixes = pref;
		imports = i;
	}
}
