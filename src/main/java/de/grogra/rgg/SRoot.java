package de.grogra.rgg;

import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.reflect.XClass;
import de.grogra.reflect.XObject;

public class SRoot extends Node {

	private static final long serialVersionUID = 5273611604396944403L;

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new SRoot ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new SRoot ();
	}

//enh:end

}
